# A Framework for Model-Agnostic Evaluation of Machine Learning Models based on Clustering and Progressive Visualization

(We may come up with a proud name, some thing like PROSTER (from **PRO**gressive and cl**USTER**ing))


## Author
Oscar Alexander Kirschstein Schäfer

Supervised by Prof. Michael Behrisch, (second formal supervior from the AI department), and Yuncong Yu


## Abstract

(Please add your teaser image and abstract here to arouse interest)


## Links

- KonJoin: https://uu.konjoin.nl/project/1170-a-framework-for-model-agnostic-evaluation-of-machine-learning-models-based-on-clustering-and-progressive-visualization
- Overleaf: https://www.overleaf.com/project/636e29323370e40637fad584 (MoSCoW in Section 5 Goals)
- Gantt chart for project management: https://view.monday.com/1136234174-e2c0f662aea0cdd14d70d9b43b9dfa3e?r=euc1


## Installation

(Please tell user how to install your tool including dependencies.)


## Usuage Examples

(Please provide some examples showing how to use your tool including some representative results.)


## API References

(Please list APP APIs for future developers.)


## Miscellaneous

(Please list the common problems and other nice-to-knows here.)
