Timestamp	Transcription
00:00.960	Yes, okay, we're going.
00:03.379	So, task one.
00:05.219	The task one would be to look at the model cluster performance metrics and find which the best and the worst performing models are.
00:14.179	Okay, so there is a couple of ways I could go into that, like especially the looking at totals in this row.
00:25.460	I'm pointing the first table and the last row has the total results.
00:35.399	So, I'm making the assumption that that's the overall performance.
00:40.399	So, I can say that here linear regression and ridge regression are pretty good and in conscious LSTM is pretty bad.
00:52.320	So, if I want to specify that, okay, actually, let me just ridge regression and this one should show me specifically for ridge regression all the points and what I'm doing is applying the filter for both linear regression to the right and ridge regression checking that the clusters, okay, ridge regression, linear regression.
01:18.040	Oh, did they have the same clusters lit up?
01:20.620	Cluster two and let me check differently slightly.
01:24.159	Maybe this shows me a little bit better.
01:26.799	Okay, if I just remove this to see, okay, so linear regression and ridge regression.
01:35.120	Okay, it's still cluster two and cluster five that are like being a little bit displayed.
01:39.040	Some of cluster three is here also being displayed.
01:42.700	If I want to check specifically what that is, I'm going onto the DR method and checking specifically for these clusters, cluster two.
01:52.700	Okay, this is like the number here, it feels kind of arbitrary.
01:59.840	Is that like just a percentage?
02:01.939	Or like the Fahrenheit?
02:04.200	Okay, okay.
02:05.359	Yeah, because I can understand what it says at the bottom where it says the date specifically, but it just says 100.
02:12.879	So, the assumption for that is is that like how you did it here for to the right, these bars where they say it's Fahrenheit.
02:21.879	I like that this is pretty good.
02:23.759	But how does that compare next to like colors and Fahrenheit?
02:29.939	Here, root mean square for Fahrenheit.
02:34.219	But a person may look at that and be like, okay, this is just like how many Fahrenheit's that is.
02:41.400	Because somebody may not know what root mean square it is, but you can recognize the Fahrenheit and make the assumption, okay, I can say that green could be cold, but red is definitely very warm.
02:55.659	And like the warmer it is, and this is here at L, oh, you can also move it this way.
03:00.479	Okay, pretty cool.
03:01.180	So let me just put it back how it used to be.
03:04.280	The LSTM, oh, this is a pretty high number.
03:07.159	So I can assume this is good.
03:08.840	But that's just like, that's bad, you know, that's not what you're supposed to see.
03:13.620	So like, so every time you look at some like a dashboard and like these graphs, the first thing you look at are the like the leftmost, the leftmost side, and then top or bottom based on where the like where the pointers are put in.
03:37.020	And here it's presented pretty well.
03:40.120	I can say that sorry, but like, we're not going and doing the task.
03:43.599	I'm just giving you some hard criticism.
03:46.080	That's good, though.
03:47.099	Nice feedback.
03:47.860	I wanted to say also the screen is also recording.
03:50.479	So if you want to say like, I'm okay, okay, yeah, okay.
03:54.219	So this here like, like cognitively, the first thing we look at is like the left side and like the top.
04:00.960	So you instantly like understand what the thing that's being displayed here.
04:05.539	But like these numbers inside of it are just like quite arbitrary.
04:08.900	But if I look here to the right, I can see root mean square error.
04:12.319	And, you know, because I am in this field, I do understand what that means.
04:16.379	But like, I just feel like somebody could look at this and be like, okay.
04:19.680	And also, for colorblind people, that may be a problem because this screen and this screen could basically be the same color for them.
04:27.399	And I see how you're putting down the numbers, but some people simply could not see those colors.
04:33.160	Okay, let's continue with task two, okay.
04:38.139	Okay.
04:38.560	Your feedback is really appreciated, though.
04:41.759	So now what you should do is select a cluster that you deem problematic and locate it both on the cluster graph and the dimensionality reduction graph.
04:53.560	Okay, let me put it back to the original one.
04:56.660	Here, the one that's sticking out to me as being pretty bad is what is this?
05:03.560	This is cluster three, just because it has like a massive gap where values are separated like on two other, like different extremes.
05:12.060	But I can look at it a little bit better just to see what they're, wait, that's three, not two, three, okay.
05:19.980	Okay, this looks decent.
05:22.620	But if I give it a different representation, oh, it's still bad.
05:27.019	Yeah, that's say that, let me check.
05:29.720	Oh, this one feels a little bit better, but I still see values being like clustered still into two categories, one at the very bottom and one at the very top.
05:38.620	Even though now currently, cluster five looks also pretty extreme, where all of its values are like at the very, very ends of it, with a couple of them being in the middle.
05:48.699	cluster two seems pretty good.
05:51.399	cluster three, also some extremes could be pointed out.
05:56.319	However, cluster, let me see.
05:59.199	Now, there is some also overlap between clusters one and cluster four.
06:05.500	And, you know, if like the model was perfect, it can always like, I mean, perfect, that's very, I don't think nothing is perfect, but there are some values that overlap with both of them.
06:17.540	And even if I go into, yes, still overlap and still overlap, yeah, that's interesting.
06:25.300	Okay.
06:26.019	So now that you have located the cluster also, please, with the knowledge I gave you about the dataset, and looking at both the cluster plot and the stats, try to give it a semantic meaning.
06:38.279	So try to give the cluster a semantic meaning.
06:41.560	Okay.
06:43.100	Okay.
06:44.759	Okay, that's, that's a very, very interesting question.
06:47.680	So what can I get out of this?
06:49.980	If I specify this cluster specifically, if I look at the values here and actually let me look at it a little bit, a little bit better.
06:59.680	So let me like create a zoom here.
07:01.399	So I see what kind of values there are in both of them.
07:04.040	And okay, this is a slight issue.
07:07.259	I mean, I see that cluster one, cluster one, cluster one.
07:12.079	How?
07:13.019	Oh, okay, that's how.
07:14.379	So there are some overlapping values.
07:17.220	And I believe that may be causing the issue here.
07:21.220	And also like I cannot properly see it, even though this color is pretty good, but like, it's very like, white on white.
07:29.759	So I don't see them like really, really properly.
07:33.220	But like, let's just see, let's just see.
07:35.300	So circles are supposed to be in, is this how you move over?
07:40.899	Okay, shit.
07:45.000	All right.
07:46.360	Okay.
07:47.100	So, okay, pluses are supposed to be in red, while this is supposed to be in blue.
07:51.500	I mean, there's some overfitting here.
07:54.160	There's a little bit of trying to do everything.
07:57.939	And then just not actually underfitting.
08:01.579	Because you're like, if it were to overfit, then you would fully have a pretty good separation between all of them for each one of the values.
08:10.120	And it's a little bit underfitted.
08:12.459	But I would not know why.
08:16.100	Because like, I'm not quite sure what you're trying to accomplish with all of these models.
08:22.000	And like, are they all just like doing the same thing?
08:26.439	Is it is this representation like of every single model?
08:31.439	And this is like the general that all of them brought out?
08:34.879	Or would it be something like, this is just the data that we have, and it's plotted, and this is how they we have clustered it?
08:42.200	Exactly.
08:43.259	What is done first is clustering the data to make similar groups of data be together, to be able to give an intuition as to how the data looks, and then how this shape of data may affect performance.
09:00.779	That's why this graph is there.
09:03.259	Okay.
09:03.539	Yeah.
09:03.840	To see that.
09:04.580	And then here, you can see a more fine grained version of the clusters in this case, low dimensional space of 2D space.
09:13.659	Yeah.
09:13.980	So, you can zoom in on the more problematic instances also, for example.
09:18.759	But yeah, let's Oh, by the way, also this as well.
09:22.879	What does do these numbers mean?
09:26.820	Is it just the value of Yeah, because I could it's just left there as like zero point something.
09:32.159	Yeah, I skipped that before, because I didn't deem it important.
09:35.879	But what that is is basically the silhouette score, which is a quality metric for clustering.
09:41.559	Yeah.
09:42.139	So in this case, paired with dimensionality reduction.
09:45.299	So the higher the higher the number, it's supposed to be the better or the better according to Oh, there is no way you can separate this.
09:55.039	So yeah, okay.
09:56.440	Because I see there's some like values that are genuinely just in both of them, well, and like surrounded by both of them.
10:03.700	So this is like the best it can do.
10:05.519	So I understand that.
10:07.259	Okay, let's return to the task though.
10:09.620	So it was you would have to try to give me all the cluster you selected for this task.
10:16.740	Yeah, the semantic meaning of the cluster.
10:20.279	Okay, so how the data looks in the cluster?
10:23.299	What what what may it be?
10:25.580	You know, so it overlaps.
10:28.419	Okay, semantic would have to do more with like the correlation to the real world.
10:35.399	So in this case, it's temperatures, right?
10:37.159	Yeah.
10:37.919	So how, how could I interpret it?
10:40.820	How could I link it to the real world, like these these instances from the cluster you chose?
10:46.220	Yeah, so like, how are you clustering these things?
10:50.659	Because you said that all of these are values, temperature values of, I guess this like one specific place over, because these are not over a couple of years, this is the same year.
11:02.419	So for each one of the clusters, this time series is from a city, from one international city, from one city, yeah, international.
11:12.679	Yeah, okay, okay, okay, okay, okay, that okay.
11:15.679	So I would say these two cities could be closer, because they share similar temperatures on similar days.
11:24.600	And that could mean that they just, they just have the same temperature or like are similarly tropical.
11:33.080	And if I look at cluster one and cluster five, these two graphs do correlate quite a bit.
11:38.379	So like that could make an assumption that the climates in those two, two different cities are very, very similar.
11:47.899	If not, let me check, let me check, they are basically identical.
11:52.200	Okay, let me check.
11:53.259	Okay, no, they're not identical, but they're really, really close.
11:56.940	They're really, really close, especially this curb slightly here.
12:01.659	So okay, that could only, I mean, the correlation does not mean causation here, because like that's just maybe the year was the same, like for both of them.
12:12.720	So they had a very, very similar graph.
12:16.100	Okay, okay, that's it for our task two.
12:18.539	Thank you.
12:18.820	And now let's go to last task, which is now you found the bad performing cluster.
12:26.340	Now you have to try to find the worst performing model cluster pair by looking at the matrix first.
12:34.179	Okay, the worst performing one here that I see based on the big number is the the LSDM one and the simple native ones.
12:50.059	But just because they're close does not mean they're coupled.
12:56.200	I was referring to not a pair of models, but model cluster pair.
13:02.480	So okay, let me select model.
13:06.659	And here I see that the worst one actually is LSDM and cluster four.
13:12.620	So cluster four, LSDM.
13:17.779	Oh, wow, okay.
13:19.980	Yeah, okay, see how that's bad.
13:22.139	It's just a single line that you can just pull from here to, sorry, from here to here, they all share like the most amount of values.
13:32.679	And I don't think that's how it's supposed to be.
13:35.139	Let me just look at it from different perspectives.
13:37.720	Yeah, I see, okay, it's not a line anymore, but it's just an arc being all the way here.
13:41.720	And I feel like these, okay, these scores are really bad.
13:45.460	Okay, this leads us to the next step in the task, which is now with this information, filter the dimension anti reduction plot by selecting the according cluster model, you've already done that.
13:57.019	Good job.
13:58.220	Inside this cluster, find any instances that are performing noticeably worse than the rest, select them and try to give them a meaning also.
14:08.019	Okay, let me, this is pretty bad.
14:10.600	Let me actually select the whole cluster.
14:13.179	This is roughly how big it is.
14:15.139	This is roughly how small it is.
14:17.220	So I've selected only the cluster.
14:18.960	I'm going to look at the values now.
14:20.759	The ones at the bottom are pretty good.
14:22.720	So I'm making the assumption opposite and pretty bad.
14:26.019	That is it.
14:26.919	Oh, this was pretty bad.
14:28.259	And the ones that are more red are always, always bad based on the thing that I see on the side here.
14:35.080	And I have, yeah, I'd say these ones with the scores of 93 .95 and this one 92 .91 would be the worst performing ones.
14:48.460	I think even like overall, like, I don't think that scores can even be that big.
14:54.120	Okay.
14:55.000	Yeah, it's like, and now also try to compare them with the total instances of that cluster.
15:01.139	So you can use the shape, the color, the performance score.
15:06.360	So, okay, okay, let me just see what I can do with the other ones.
15:11.460	No, I don't like this one.
15:15.120	Okay, maybe no.
15:18.159	Let's go to the DOG one.
15:20.799	So I'm going to do lasso select, just to select every single instance from here.
15:30.539	Being like, okay, so, okay.
15:34.460	Selection.
15:37.919	This should, this play, okay, the graph here is pretty uneven.
15:46.279	I'd say that if I actually look at these values here specifically, it goes from y minus 18 to plus 42.
15:57.340	And I don't think that it should be that diverse.
16:00.840	I believe that it should just be closer to either the medium range or just like some middle point.
16:14.240	And it's not currently doing that.
16:15.519	It's being all over the place.
16:18.340	Especially cluster four.
16:20.500	Wow.
16:20.919	Yeah, that's the one I've selected.
16:22.299	Yeah.
16:23.399	Yeah, so the task was actually to select the most problematic instances inside.
16:33.000	Ah, okay, okay, okay.
16:34.039	So let me just, okay, see if I can just select cluster four here.
16:39.100	Let me see if I can like isolate the worst ones by themselves.
16:45.419	Yeah, this one allows me to like just cluster the better ones are up to the left than the worst ones are up here.
16:51.919	So I'm using lasso to only select the worst performing ones, even though I'm kind of going into blue as well, but that should be fine.
17:01.240	And even with this selection, the selected values, even though they kind of follow with the the cluster four one, they're like even like more sporadic, just like big extremes that I'm seeing here.
17:22.359	Okay.
17:23.359	So how would you compare it to the general cluster or to the rest?
17:29.880	To the rest.
17:30.859	So this in my humble opinion, this is more sporadic and it's not like, oh, wait, let me see selection.
17:38.359	Okay, there's also, oh, the variance is a little too high in my opinion.
17:42.599	Standard derivation also.
17:46.099	I don't know what these two are, but I'm just going to say based on variance, the median values and everything else that I see here.
17:55.400	This ain't it chief.
17:57.319	Okay.
17:58.240	I'm terribly sorry.
17:59.640	I probably should have used better language, however.
18:01.960	Exactly.
18:02.779	Yeah, I just I can I can see but I'm not quite sure what is causing them to be this bad because, you know, I'm not doing climate stuff.
18:15.539	I'm mostly just generating these models that get fed a bunch of information.
18:20.900	So they make graphs like this.
18:23.720	Okay.
18:24.359	Yeah.
18:25.119	Then this was it for the task.
18:26.920	Thank you for your input.
