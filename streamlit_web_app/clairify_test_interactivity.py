from config import *
from visualization import *
from streamlit_plotly_events import plotly_events

DEBUG = True

def clAIrify_dashboard():
    st.header('ClAIrify - Dynamic')
    if DEBUG == True: print('Redraw dashboard')
    dimred_fig = dimred_plot(st.session_state["performance_score_data"], st.session_state["cluster_labels"], st.session_state["selections"])
    dimred_selection = plotly_events(dimred_fig, select_event=True, click_event=False)
    st.write(dimred_selection)
    update_state(dimred_selection)
 
@st.cache_data # Supposed to be run only once
def initialize_state():
    # Test
    if DEBUG == True: print("Initialize state")

    st.session_state["raw_data"] = pd.read_pickle('../Dataset/preprocessed/weather/X.pkl')
    st.session_state["performance_score_data"] = pd.read_pickle('../Dataset/performance_scores/error_weather.pkl')

    # Clustering
    cluster_labels, cluster_count = clustering(st.session_state["raw_data"], n_clusters=N_CLUSTERS_DEFAULT)

    st.session_state["cluster_labels"] = pd.read_pickle('../Dataset/cluster_data/ts_weather_clusters.pkl')
    st.session_state["cluster_count"] = 5

    # Embeddings
    st.session_state["embedding"] = pd.read_pickle('embeddings.pkl')

    # Identifier - to identify the selected data instances from the dimensionality reduction plot
    coord_id = np.char.add((st.session_state["embedding"][:, 0]*10e4).astype(int).astype(str),np.char.add("-",(st.session_state["embedding"][:, 1]*10e4).astype(int).astype(str)))
    st.session_state["identifier"] = pd.Series(coord_id) # Identifiers consistent with raw data indexes

    # Selections a series of booleans to show which instances have been selected. All of them have, in the beginning
    st.session_state["selections"] = pd.Series(np.ones(len(st.session_state["identifier"])),dtype=bool)


    # Initialize the parameter to check the selections in the dimensionality reduction plot
    st.session_state['dimred_selection'] = None

    return None

def reset_filters():
    st.session_state["selections"] = pd.Series(np.ones(len(st.session_state["identifier"])),dtype=bool)


# Function to take in to account all the filters that have been applied to the data
# It also serves to know if we have to rerun the app
def update_state(dimred_selection):
    
    # if DEBUG == True: print(st.session_state['dimred_selection'], "-----", dimred_selection)

    if st.session_state['dimred_selection'] is None:
        if DEBUG == True: print('Initialize selection')
        st.session_state['dimred_selection'] = dimred_selection
        return
    elif st.session_state['dimred_selection'] == dimred_selection: # If the selection is the same, then do nothing
        if DEBUG == True: print('equal')
        return
    
    # If the code reaches here, there has been a new filter applied
    if len(dimred_selection) == 0: #If nothing was selected, nothing happens
        return
    else:
        if DEBUG == True: print('New selection')
        st.session_state['dimred_selection'] = copy.deepcopy(dimred_selection) # Store the current selection

        selection_df = pd.DataFrame(st.session_state['dimred_selection'])

        selections = ((selection_df['x']*10e4).astype(int).astype(str) + '-' + (selection_df['y']*10e4).astype(int).astype(str))
        st.session_state["selections"] = st.session_state["identifier"].isin(selections)

        if DEBUG: print(f"Number of selections : {sum(st.session_state['selections'])}")
    st.experimental_rerun()

def test_plotly_events():
    np.random.seed(123)
    x = np.random.rand(100)
    y = np.random.rand(100)
    color = np.random.rand(100)

    # Create scatter plot
    fig = px.scatter(x=x, y=y, color=color)
    
    selected_points = plotly_events(fig, select_event=True, click_event=False)

    st.write(selected_points)

def app():
    # Set the page title
    st.set_page_config(layout="wide", page_title='ClAIrify')

    _ = initialize_state()

    # test_plotly_events()

    
    # if 'data_instances' not in st.session_state or 'performance_scores' not in st.session_state:
    #     upload_data()
    # else:
    clAIrify_dashboard()

if __name__ == '__main__':
    app()
