import streamlit as st

import numpy as np
import pandas as pd
pd.options.plotting.backend = "plotly"
import math
import seaborn as sns
import umap

import matplotlib.pyplot as plt
from matplotlib.colors import TABLEAU_COLORS
import matplotlib.markers as markers
from matplotlib.lines import Line2D

from tslearn.barycenters import dtw_barycenter_averaging
from tslearn.clustering import TimeSeriesKMeans

from scipy.spatial import ConvexHull
from scipy import stats

from sklearn.preprocessing import StandardScaler

import plotly.graph_objects as go
import plotly.express as px
from plotly.subplots import make_subplots

import statsmodels.api as sm

import copy


N_CLUSTERS_DEFAULT = 5

COLOR_SCALE = 'balance'

MARKER_SHAPES = list(markers.MarkerStyle.markers.keys())[2:37]
CLUSTER_COLORS = {cluster_id:color for cluster_id, color in zip(range(len(TABLEAU_COLORS)), TABLEAU_COLORS.values())}
CLUSTER_POINT_SHAPE = {cluster_id:shape for cluster_id, shape in zip(range(len(MARKER_SHAPES)), MARKER_SHAPES)}


TMP_FOLDER = "./tmp/"
TMP_CLUSTER_FILE = TMP_FOLDER + "clusters.pkl"