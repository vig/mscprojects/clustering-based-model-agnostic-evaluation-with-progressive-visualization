from config import *

DEFAULT_PLOTLY_CLUSTER_COLORS=['rgb(31, 119, 180)', 'rgb(255, 127, 14)',
                       'rgb(44, 160, 44)', 'rgb(214, 39, 40)',
                       'rgb(148, 103, 189)', 'rgb(140, 86, 75)',
                       'rgb(227, 119, 194)', 'rgb(127, 127, 127)',
                       'rgb(188, 189, 34)', 'rgb(23, 190, 207)']
DEFAULT_PLOTLY_FILL_CLUSTER_COLORS=['rgba(31, 119, 180, 0.2)', 'rgba(255, 127, 14, 0.2)',
                       'rgba(44, 160, 44, 0.2)', 'rgba(214, 39, 40, 0.2)',
                       'rgba(148, 103, 189, 0.2)', 'rgba(140, 86, 75, 0.2)',
                       'rgba(227, 119, 194, 0.2)', 'rgba(127, 127, 127, 0.2)',
                       'rgba(188, 189, 34, 0.2)', 'rgba(23, 190, 207, 0.2)']

# Clustering plot ###########################################################################
@st.cache_data
def clustering(raw_ts_data, n_clusters):
    # Number of clusters k is the square root of the number of points in the training data
    cluster_count = math.ceil(math.sqrt(len(raw_ts_data))) 
    cluster_count = min(n_clusters,cluster_count)
    km = TimeSeriesKMeans(n_clusters=cluster_count, metric="dtw")

    cluster_labels_weather = km.fit_predict(raw_ts_data)
    pd.Series(cluster_labels_weather).rename("cluster id").to_pickle(TMP_CLUSTER_FILE)

    return cluster_labels_weather, cluster_count

# @st.cache_data
# def ts_cluster_plot(raw_ts_data, n_clusters=N_CLUSTERS_DEFAULT):
#     cluster_labels_weather, cluster_count = clustering(raw_ts_data, n_clusters)

#     fig, axs = plt.subplots(cluster_count,1, figsize=(15,cluster_count*7.5))
#     for cluster in range(cluster_count):
#         idxs = np.where(cluster_labels_weather==cluster)[0]
#         cluster_data = raw_ts_data.iloc[idxs].T
#         cluster_data.plot(ax = axs[cluster], color='grey', legend=False, alpha=0.2)
#         barycenter = dtw_barycenter_averaging(cluster_data.T, max_iter=5).flatten()
#         pd.DataFrame(barycenter, index = cluster_data.index).plot(ax = axs[cluster], color='blue', legend=False)
#         axs[cluster].set_ylabel(f"Cluster {cluster+1}")

#     return fig
    
@st.cache_data
def ts_cluster_plot(raw_ts_data, n_clusters=N_CLUSTERS_DEFAULT):
    cluster_labels_weather, cluster_count = clustering(raw_ts_data, n_clusters)

    lower_bound = raw_ts_data.min().min()
    higher_bound = raw_ts_data.max().max()

    #Convert Period objects to timestamps for PyPlot to be able to process them
    raw_ts_data.columns = raw_ts_data.columns.to_timestamp()

    fig = make_subplots(rows=cluster_count, cols=1, vertical_spacing=0.015, shared_xaxes=True,)

    for cluster in range(cluster_count):
        idxs = np.where(cluster_labels_weather==cluster)[0]
        cluster_data = raw_ts_data.iloc[idxs]
        # print(f"CLuster {cluster+1} - {cluster_data.shape}")
        # Plot each raw TS

        for idx, row in cluster_data.iterrows():
            fig.add_trace(go.Scatter(x=cluster_data.columns, y=row, line_color='rgba(30, 30, 30, 0.1)'),row=cluster+1, col=1)
        # Plot each cluster identifier
        barycenter = dtw_barycenter_averaging(cluster_data.T, max_iter=5).flatten()
        fig.add_trace(go.Scatter(x=cluster_data.columns, y=row, line=dict(color=DEFAULT_PLOTLY_CLUSTER_COLORS[cluster], width=7)),row=cluster+1, col=1)
        fig.update_yaxes(range=[lower_bound, higher_bound], row=cluster+1, col=1, title=f"Cluster {cluster+1}", title_font_family="Arial", title_font_size=24, title_font_color=DEFAULT_PLOTLY_CLUSTER_COLORS[cluster]) 

    fig.update_layout(height=700,margin=dict(l=0, r=10, t=10, b=20))
    fig.update_traces(showlegend=False)

    return fig


def helper_ts_cluster_plot():
    raw_ts_data = pd.read_pickle("../Dataset/preprocessed/weather/X.pkl")
    return ts_cluster_plot(raw_ts_data, n_clusters=N_CLUSTERS_DEFAULT)


# Dimensionality reduction plotting #########################################################

# def plot_cluster_hulls(points, cluster_labels, error_score, n_clusters):
#     fig, ax = plt.subplots(figsize=(20,10))
#     legend_elements = []

#     for cluster_id in range(n_clusters):
#         cluster_points = points[cluster_labels==cluster_id,:]
#         hull = ConvexHull(cluster_points)
#         cluster_errors = error_score[cluster_labels==cluster_id]
#         sns.scatterplot(x=cluster_points[:,0], y=cluster_points[:,1], hue=cluster_errors, palette='magma', legend=False, marker=CLUSTER_POINT_SHAPE[cluster_id])
#         for simplex in hull.simplices:
#             plt.plot(cluster_points[simplex, 0], cluster_points[simplex, 1], c=CLUSTER_COLORS[cluster_id])
            
#         plt.fill(cluster_points[hull.vertices,0], cluster_points[hull.vertices,1], alpha=0.1, c=CLUSTER_COLORS[cluster_id])
#         plt.tick_params(left = False, right = False , labelleft = False ,
#                 labelbottom = False, bottom = False)

#         #Add legend information
#         legend_elements.append(Line2D([0], [0], marker=CLUSTER_POINT_SHAPE[cluster_id], color=CLUSTER_COLORS[cluster_id], label=f'Cluster {cluster_id+1}',
#                           markerfacecolor=CLUSTER_COLORS[cluster_id], markersize=10, linestyle='None'),)
    
#     # Add color bar
#     sm = plt.cm.ScalarMappable(cmap='magma', norm=plt.Normalize(vmin=min(error_score), vmax=max(error_score)))
#     sm._A = []
#     ticks = np.linspace(error_score.min(), error_score.max(), 5, endpoint=True)
#     plt.colorbar(sm, label='RMSE', ticks=ticks, orientation='vertical', ax=ax)

#     #Add legend
#     ax.legend(handles=legend_elements, loc='upper right')

#     return fig

# @st.cache_data
# def ts_dimred_plot(raw_ts_data, performance_score_data, n_clusters = N_CLUSTERS_DEFAULT):
#     rmse_weather = performance_score_data.mean(axis=1).values
#     reducer = umap.UMAP()
#     scaled_weather_data = StandardScaler().fit_transform(raw_ts_data)
#     embedding_weather = reducer.fit_transform(scaled_weather_data)
#     weather_clusters = pd.read_pickle(TMP_CLUSTER_FILE).values
#     fig = plot_cluster_hulls(embedding_weather, weather_clusters, rmse_weather, n_clusters)
#     # fig.suptitle('UMAP projection of weather TS clusters')
#     plt.gca().set_aspect('equal', 'datalim')
#     return fig

@st.cache_data
def ts_dimred_plot(raw_ts_data, performance_score_data, clusters):
    performance_score_data = performance_score_data.mean(axis=1).values

    # Create embeddings
    reducer = umap.UMAP()
    scaled_weather_data = StandardScaler().fit_transform(raw_ts_data)
    embedding_weather = reducer.fit_transform(scaled_weather_data)

    # Create a scatterplot with different colors for each cluster
    dimred_fig = px.scatter(x=embedding_weather[:,0], y=embedding_weather[:,1], 
                            color=performance_score_data, 
                            color_continuous_scale =COLOR_SCALE, 
                            symbol=clusters, 
                            hover_name=[f'Score: {performance_score:.2f}<br>Cluster: {cluster_id+1}' for performance_score, cluster_id in zip(performance_score_data, clusters)])
    dimred_fig.update_traces(hovertemplate='', marker=dict(colorscale=COLOR_SCALE))


    # Compute the convex hulls of each cluster and add them to the plot
    for label in np.unique(clusters):
        points = embedding_weather[clusters == label]
        hull = ConvexHull(points)
        hull_points = np.append(points[hull.vertices], [points[hull.vertices[0]]], axis=0)
        dimred_fig.add_trace(go.Scatter(x=hull_points[:,0], y=hull_points[:,1], 
                        mode='lines', fill='toself', line_color=DEFAULT_PLOTLY_CLUSTER_COLORS[label], fillcolor=DEFAULT_PLOTLY_FILL_CLUSTER_COLORS[label],hoverinfo='skip'))
    dimred_fig.update_traces(showlegend=False)
    dimred_fig.update_layout(yaxis_visible=True, yaxis_showticklabels=False, yaxis_title='',xaxis_visible=True, xaxis_showticklabels=False, xaxis_title='')
    dimred_fig.update_layout(height=300,margin=dict(l=0, r=0, t=10, b=10))
    return dimred_fig

def helper_ts_dimred_plot():
    raw_ts_data = pd.read_pickle('../Dataset/preprocessed/weather/X.pkl')
    performance_score_data = pd.read_pickle('../Dataset/performance_scores/error_weather.pkl')
    clusters = pd.read_pickle('../Dataset/cluster_data/ts_weather_clusters.pkl').values

    return ts_dimred_plot(raw_ts_data, performance_score_data, clusters)

# Performance matrix plot ###################################################################
@st.cache_data
def performance_matrix_plot(cluster_labels, performance_score_data, dimred_fig):
    clusters = np.unique(cluster_labels)

    cluster_performance_data_weather = {model: [] for model in performance_score_data.columns}

    for cluster in clusters:
        cluster_data = performance_score_data.iloc[np.where(clusters==cluster)]
        for model, metric in cluster_data.mean().items():
            cluster_performance_data_weather[model].append(metric)

    cluster_performance_matrix_weather = pd.DataFrame(cluster_performance_data_weather, index = [f"Cluster {cluster_id+1}" for cluster_id in clusters])
    cluster_performance_matrix_weather = pd.concat([cluster_performance_matrix_weather.T,cluster_performance_matrix_weather.mean().rename('TOTAL')], axis=1).T
    cluster_performance_matrix_weather = cluster_performance_matrix_weather.sort_values(by='TOTAL', axis=1, ascending=True)
    cluster_performance_matrix_weather['TOTAL'] = cluster_performance_matrix_weather.mean(axis = 1)

    # Make the tooltip text
    tooltip_text = []
    n_clusters, models = cluster_performance_matrix_weather.shape
    for c in range(n_clusters):
        row = []
        for m in range(models):
            cluster_info = f'Cluster {c+1}' if c < n_clusters-1 else 'Cluster Total'
            model_info = f'{cluster_performance_matrix_weather.columns[m]}' if c < models-1 else 'Model Total'
            row.append(cluster_info + "<br>" + model_info + "<br>" + f"Score: {cluster_performance_matrix_weather.iloc[c,m]:.2f}")
        tooltip_text.append(row)
    # Create heatmap with the same colorscale as the scatter plot
    clusmod_fig = go.Figure(data=[go.Heatmap(z=cluster_performance_matrix_weather, x=cluster_performance_matrix_weather.columns, y=cluster_performance_matrix_weather.index,
                                    colorscale=dimred_fig.data[0].marker.colorscale)])
    clusmod_fig.update_traces(hovertemplate='%{text}', text=tooltip_text)

    clusmod_fig.update_layout( 
                    xaxis=dict(title='Models'), yaxis=dict(title='Clusters'))

    clusmod_fig.update_layout(xaxis_side='top', yaxis_autorange='reversed')
    clusmod_fig.update_layout(yaxis_title='', xaxis_title='')

    clusmod_fig.update_layout(height=75*len(clusters),margin=dict(l=0, r=0, t=10, b=10))

    return clusmod_fig

def helper_performance_matrix_plot(dimred_fig):
    clusters = pd.read_pickle('../Dataset/cluster_data/ts_weather_clusters.pkl').values
    performance_score_data = pd.read_pickle('../Dataset/performance_scores/error_weather.pkl')
    return performance_matrix_plot(clusters, performance_score_data, dimred_fig)