from config import *
from visualization_static import *


def upload_data():
    st.header('ClAIrify')
    st.subheader('Please upload the necessary data to start analysis.')

    # Display the title in the center of the page
    uploaded_data_instances = None
    uploaded_performance_scores = None

    # Add the file upload widgets
    
    uploaded_data_instances = st.file_uploader('Upload raw data instances', type='pkl')
    uploaded_performance_scores = st.file_uploader('Upload performance scores', type='pkl')

    # Read the uploaded files into session state
    if uploaded_data_instances:
        data_instances = pd.read_pickle(uploaded_data_instances)
        st.session_state['data_instances'] = data_instances

    if uploaded_performance_scores:
        performance_scores = pd.read_pickle(uploaded_performance_scores)
        st.session_state['performance_scores'] = performance_scores

    st.button('Continue')

    # if uploaded_data_instances and uploaded_data_instances:
    #     st.pyplot(ts_cluster_plot(st.session_state['data_instances']))


def clAIrify_dashboard():
    st.header('ClAIrify - Static')

    col1, col2 = st.columns([1,2])

    with col1:
        # st.pyplot(ts_cluster_plot(st.session_state['data_instances']))
        st.plotly_chart(helper_ts_cluster_plot(), use_container_width=True)

    with col2:
        dimred_fig = helper_ts_dimred_plot()
        st.plotly_chart(dimred_fig, use_container_width=True)
        st.plotly_chart(helper_performance_matrix_plot(dimred_fig), use_container_width=True)
    st.button('Reload Test')
    


def app():
    # Set the page title
    st.set_page_config(layout="wide", page_title='ClAIrify')
    # if 'data_instances' not in st.session_state or 'performance_scores' not in st.session_state:
    #     upload_data()
    # else:
    clAIrify_dashboard()


if __name__ == '__main__':
    app()
