from config import *
from visualization import *
from streamlit_plotly_events import plotly_events

DEBUG = True

#Function that processes all the loading of data (cached because its meant to be run only once)
@st.cache_data
def upload_data():
    st.header('ClAIrify')
    st.subheader('Please upload the necessary data to start analysis.')

    # Display the title in the center of the page
    uploaded_data_instances = None
    uploaded_performance_scores = None

    # Add the file upload widgets
    
    uploaded_data_instances = st.file_uploader('Upload raw data instances', type='pkl')
    uploaded_performance_scores = st.file_uploader('Upload performance scores', type='pkl')

    # Read the uploaded files into session state
    if uploaded_data_instances:
        data_instances = pd.read_pickle(uploaded_data_instances)
        st.session_state['data_instances'] = data_instances

    if uploaded_performance_scores:
        performance_scores = pd.read_pickle(uploaded_performance_scores)
        st.session_state['performance_scores'] = performance_scores

    st.button('Continue')

    # if uploaded_data_instances and uploaded_data_instances:
    #     st.pyplot(ts_cluster_plot(st.session_state['data_instances']))

def explore_dashboard():
    col1, col2 = st.columns([1,2])

    with col2:
        dimred_fig = dimred_plot(st.session_state["performance_score_data"], st.session_state["cluster_labels"], st.session_state["selections"], st.session_state["selected"])
        dimred_selection = plotly_events(dimred_fig, override_height=dimred_fig.layout.height, select_event=True, click_event=False)

        update_state(dimred_selection)

        permat_fig = performance_matrix_plot(st.session_state["cluster_labels"], st.session_state["performance_score_data"], dimred_fig)
        st.plotly_chart(permat_fig, use_container_width=True)
    with col1:
        # st.pyplot(ts_cluster_plot(st.session_state['data_instances']))
        clusterplot_fig = cluster_plot(st.session_state["raw_data"], n_clusters=N_CLUSTERS_DEFAULT)
        st.plotly_chart(clusterplot_fig, use_container_width=True)

def specific_dashboard():

    if not st.session_state['selected']:
        st.write('Select points on the scatter plot or a cluster from the sidebar to analyze them here more specifically.')
        return
    
    # First do a plot of all the instances above, and get the barycenter average
    print(len(st.session_state["raw_data"]), " ", len(st.session_state["selections"]))
    selection = st.session_state["raw_data"].loc[st.session_state['selections'].values]
    st.plotly_chart(selection_plot(selection), use_container_width=True)
    # Secondly show a table with descriptive statistics
    st.write("Placeholder for stats")
    descriptive_stats_selection(selection)

# To set the GUI of the settings sidebar
def set_settings():

    if not 'selected_cluster' in st.session_state:
        st.session_state['selected_cluster'] = 'All'
    
    if len(st.session_state['dimred_selection']) and st.session_state['selected_cluster'] != 'All': # In the casa that there was a dimred_selection but the cluster selection overrides it
        if DEBUG: print('Cluster selection overrides dimred selection')
    elif st.session_state['cluster_selection'] == 'All' and len(st.session_state['dimred_selection']): # This means a dimred selection was made and it was signalled to reset the cluster selection
        if DEBUG: print('Dimred selection overrides cluster selection.')
        st.session_state['selected_cluster'] = 'All'
    
    # if st.session_state['selected_cluster'] != 'All' and len(st.session_state['dimred_selection']):
    #     if DEBUG: print('Reset cluster')
    #     st.session_state['selected_cluster'] = 'All'
    #     st.session_state['cluster_selection'] = 'All'

def settings_sidebar():
    set_settings()

    with st.sidebar:
        cluster_selection = st.radio("Cluster inspection", ["All"] + [f"Cluster {c+1}" for c in range(st.session_state["cluster_count"])], key="selected_cluster")

    update_settings(cluster_selection)

#Function with the settings of the sidebar
def update_settings(cluster_selection):
    select_cluster(cluster_selection)
    

def select_cluster(cluster_selection):
    
    if st.session_state['cluster_selection'] == cluster_selection: # If nothing new has been selected, do nothing
        return
    
    if DEBUG: print(f"{st.session_state['cluster_selection']} <--> {cluster_selection}")
    
    # If the execution reaches here, something changed
    st.session_state['cluster_selection'] = cluster_selection


    # What if dimred selection is present?
    if st.session_state['dimred_selection']: # De-select the selection
        if DEBUG: print('De-selecting dimred_selection')
        st.session_state['dimred_selection'] = []
    
    if st.session_state['cluster_selection'] == "All": # If selection changed to all
        reset_selections()
    else: # If it changed to a cluster
        st.session_state['selected'] = True
        cluster = int(st.session_state['cluster_selection'].split(' ')[-1])-1
        st.session_state['selections'] = pd.Series(st.session_state["cluster_labels"] == cluster)

def clAIrify_dashboard():
    st.header('ClAIrify - Dynamic')
    if DEBUG == True: print('Draw dashboard')

    settings_sidebar()

    tab1, tab2 = st.tabs(["Explore", "Specific"])
    with tab1:
        explore_dashboard() # Regular dashboard if nothing selected
    with tab2:
        specific_dashboard()
 
def reset_selections():
    st.session_state["selections"] = pd.Series(np.ones(len(st.session_state["identifier"])),dtype=bool)
    st.session_state["selected"] = False

@st.cache_data # Supposed to be run only once
def initialize_state():
    # Test
    if DEBUG == True: print("Initialize state")

    # Load data
    st.session_state["raw_data"] = pd.read_pickle('../Dataset/preprocessed/weather/X.pkl')
    st.session_state["performance_score_data"] = pd.read_pickle('../Dataset/performance_scores/error_weather.pkl')

    # Clustering
    cluster_labels, cluster_count = clustering(st.session_state["raw_data"], n_clusters=N_CLUSTERS_DEFAULT)

    st.session_state["cluster_labels"] = cluster_labels
    st.session_state["cluster_count"] = cluster_count

    # Embeddings
    st.session_state["embedding"] = get_embeddings(st.session_state["raw_data"])

    # Dynamical selection logic #############################################

    # Identifier - to identify the selected data instances from the dimensionality reduction plot
    coord_id = np.char.add((st.session_state["embedding"][:, 0]*10e4).astype(int).astype(str),np.char.add("-",(st.session_state["embedding"][:, 1]*10e4).astype(int).astype(str)))
    st.session_state["identifier"] = pd.Series(coord_id) # Identifiers consistent with raw data indexes

    # Selections a series of booleans to show which instances have been selected. All of them have, in the beginning
    reset_selections()
    # Initialize the parameter to check the selections in the dimensionality reduction plot
    st.session_state['dimred_selection'] = []

    #Initialization of cluster selection parameter
    st.session_state['cluster_selection'] = "All"
    return None

def reset_filters():
    st.session_state["selections"] = pd.Series(np.ones(len(st.session_state["identifier"])),dtype=bool)


# Function to take in to account all the filters that have been applied to the data
# It also serves to know if we have to rerun the app
def update_state(dimred_selection):
    

    if st.session_state['dimred_selection'] == dimred_selection: # If the selection is the same, then do nothing
        if DEBUG == True: print('equal')
        return
    else:
        # If the code reaches here, there has been a new filter applied
        if len(dimred_selection) == 0: #If nothing was selected, nothing happens
            # st.session_state['dimred_selection'] = []
            return
        else: # If something was selected
            if DEBUG == True: print('New dimred selection')
            if DEBUG == True: print(st.session_state['dimred_selection'], "-----", dimred_selection)
            
            # Reset cluster selection
            st.session_state['cluster_selection'] = 'All'

            st.session_state['selected'] = True
            st.session_state['dimred_selection'] = copy.deepcopy(dimred_selection) # Store the current selection
            selection_df = pd.DataFrame(st.session_state['dimred_selection'])
            selections = ((selection_df['x']*10e4).astype(int).astype(str) + '-' + (selection_df['y']*10e4).astype(int).astype(str))
            st.session_state["selections"] = st.session_state["identifier"].isin(selections)

            # if DEBUG: print(st.session_state["identifier"], st.session_state["selections"], '=', sum(st.session_state["selections"]))
            st.experimental_rerun()

def test_plotly_events():
    np.random.seed(123)
    x = np.random.rand(100)
    y = np.random.rand(100)
    color = np.random.rand(100)

    # Create scatter plot
    fig = px.scatter(x=x, y=y, color=color)
    
    selected_points = plotly_events(fig, select_event=True, click_event=False)

    st.write(selected_points)

def app():
    # Set the page title
    st.set_page_config(layout="wide", page_title='ClAIrify')

    _ = initialize_state()

    # test_plotly_events()

    
    # if 'data_instances' not in st.session_state or 'performance_scores' not in st.session_state:
    #     upload_data()
    # else:
    clAIrify_dashboard()

if __name__ == '__main__':
    app()
