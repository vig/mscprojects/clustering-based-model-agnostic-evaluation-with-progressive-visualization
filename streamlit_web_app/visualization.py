from config import *
from pandas import DatetimeIndex

DEFAULT_PLOTLY_CLUSTER_COLORS=['rgb(31, 119, 180)', 'rgb(255, 127, 14)',
                       'rgb(44, 160, 44)', 'rgb(214, 39, 40)',
                       'rgb(148, 103, 189)', 'rgb(140, 86, 75)',
                       'rgb(227, 119, 194)', 'rgb(127, 127, 127)',
                       'rgb(188, 189, 34)', 'rgb(23, 190, 207)']
DEFAULT_PLOTLY_FILL_CLUSTER_COLORS=['rgba(31, 119, 180, 0.2)', 'rgba(255, 127, 14, 0.2)',
                       'rgba(44, 160, 44, 0.2)', 'rgba(214, 39, 40, 0.2)',
                       'rgba(148, 103, 189, 0.2)', 'rgba(140, 86, 75, 0.2)',
                       'rgba(227, 119, 194, 0.2)', 'rgba(127, 127, 127, 0.2)',
                       'rgba(188, 189, 34, 0.2)', 'rgba(23, 190, 207, 0.2)']

# Clustering plot ###########################################################################
def clustering(raw_data, n_clusters=N_CLUSTERS_DEFAULT):
    # Number of clusters k is the square root of the number of points in the training data
    cluster_count = math.ceil(math.sqrt(len(raw_data))) 
    cluster_count = min(n_clusters,cluster_count)
    km = TimeSeriesKMeans(n_clusters=cluster_count, metric="dtw")

    cluster_labels = km.fit_predict(raw_data)
    pd.Series(cluster_labels).rename("cluster id").to_pickle(TMP_CLUSTER_FILE)

    return cluster_labels, cluster_count
    
def get_embeddings(raw_data):
    # Create embeddings
    reducer = umap.UMAP()
    scaled_data = StandardScaler().fit_transform(raw_data)
    embedding = reducer.fit_transform(scaled_data)

    return embedding

@st.cache_data
def selection_plot(selection):
    
    lower_bound = st.session_state['raw_data'].min().min()
    higher_bound = st.session_state['raw_data'].max().max()

    #Convert Period objects to timestamps for PyPlot to be able to process them
    if type(selection.columns) is not DatetimeIndex:
        selection.columns = selection.columns.to_timestamp()

    # create a figure with a line trace for each time series
    fig = go.Figure()

    for idx, row in selection.iterrows():
        fig.add_trace(go.Scatter(x=selection.columns, y=row, line_color='rgba(30, 30, 30, 0.1)'))
    
    barycenter = dtw_barycenter_averaging(selection, max_iter=5).flatten()
    fig.add_trace(go.Scatter(x=selection.columns, y=barycenter.ravel(), line=dict(color=DEFAULT_PLOTLY_CLUSTER_COLORS[-1], width=7)))
    fig.update_yaxes(range=[lower_bound, higher_bound]) 

    fig.update_layout(height=350,margin=dict(l=0, r=10, t=10, b=20))
    fig.update_traces(showlegend=False)

    return fig

def descriptive_stats_selection(selection):
    data = selection.T

    col1, col2, col3, col4, col5, col6 = st.columns(6)

    mean = data.values.mean()
    col1.metric(label="Mean", value=f"{mean:.4f}")
    median = data.stack().median()
    col2.metric(label="Median", value=f"{median:.4f}")
    variance = data.values.var()
    col3.metric(label="Variance", value=f"{variance:.4f}")
    std_dev = data.values.std()
    col4.metric(label="Std. dev", value=f"{std_dev:.4f}")
    skewness = data.stack().skew()
    col5.metric(label="Skewness", value=f"{skewness:.4f}")
    kurtosis = data.stack().kurtosis()
    col6.metric(label="Kurtosis", value=f"{kurtosis:.4f}")
    # autocorrelation = data.autocorr(lag=1)
    # col7.metric(label="Autocorrelation", value=f"{autorrelation}")
    


@st.cache_data
def cluster_plot(raw_data, n_clusters=N_CLUSTERS_DEFAULT):
    cluster_labels, cluster_count = st.session_state["cluster_labels"], st.session_state["cluster_count"]

    lower_bound = raw_data.min().min()
    higher_bound = raw_data.max().max()

    #Convert Period objects to timestamps for PyPlot to be able to process them
    if type(raw_data.columns) is not DatetimeIndex:
        raw_data.columns = raw_data.columns.to_timestamp()

    fig = make_subplots(rows=cluster_count, cols=1, vertical_spacing=0.015, shared_xaxes=True)

    for cluster in range(cluster_count):
        idxs = np.where(cluster_labels==cluster)[0]
        cluster_data = raw_data.iloc[idxs]
        # print(f"CLuster {cluster+1} - {cluster_data.shape}")
        # Plot each raw TS

        for idx, row in cluster_data.iterrows():
            fig.add_trace(go.Scatter(x=cluster_data.columns, y=row, line_color='rgba(30, 30, 30, 0.1)'),row=cluster+1, col=1)
        # Plot each cluster identifier
        barycenter = dtw_barycenter_averaging(cluster_data, max_iter=5).flatten()
        fig.add_trace(go.Scatter(x=cluster_data.columns, y=barycenter.ravel(), line=dict(color=DEFAULT_PLOTLY_CLUSTER_COLORS[cluster], width=7)),row=cluster+1, col=1)
        fig.update_yaxes(range=[lower_bound, higher_bound], row=cluster+1, col=1, title=f"Cluster {cluster+1}", title_font_family="Arial", title_font_size=24, title_font_color=DEFAULT_PLOTLY_CLUSTER_COLORS[cluster]) 

    fig.update_layout(height=700,margin=dict(l=0, r=10, t=10, b=20))
    fig.update_traces(showlegend=False)

    return fig

@st.cache_data
def dimred_plot(performance_score_data, clusters, selections, selected=False):
    performance_score_data = performance_score_data.mean(axis=1).values

    embedding = st.session_state["embedding"]

    # Create a scatterplot with different colors for each cluster
    dimred_fig = px.scatter(x=embedding[:,0], y=embedding[:,1], 
                            color=performance_score_data, 
                            color_continuous_scale =COLOR_SCALE,
                            symbol=clusters, 
                            hover_name=[f'Score: {performance_score:.2f}<br>Cluster: {cluster_id+1}' for performance_score, cluster_id in zip(performance_score_data, clusters)])
    dimred_fig.update_traces(hovertemplate='', marker=dict(colorscale=COLOR_SCALE))

    # Compute the convex hulls of each cluster and add them to the plot
    for label in np.unique(clusters):
        points = embedding[clusters == label]
        hull = ConvexHull(points)
        hull_points = np.append(points[hull.vertices], [points[hull.vertices[0]]], axis=0)
        dimred_fig.add_trace(go.Scatter(x=hull_points[:,0], y=hull_points[:,1], 
                        mode='lines', fill='toself', line_color=DEFAULT_PLOTLY_CLUSTER_COLORS[label], fillcolor=DEFAULT_PLOTLY_FILL_CLUSTER_COLORS[label],hoverinfo='skip'))
        

    # Compute convex hull of selection
    points = embedding[selections]
    if selected: 
        if len(points) == 1: #Draw a circle around it
            radius = 0.3
            # define a set of points along the circumference of the circle
            theta = np.linspace(0, 2*np.pi, 100)
            x_circ = points[0,0] + radius * np.cos(theta)
            y_circ = points[0,1] + radius * np.sin(theta)
            dimred_fig.add_trace(go.Scatter(x=x_circ, y=y_circ, mode="lines", line=dict(dash="dash")))
        elif len(points) == 2: #Draw an elipse around both points
            x1, x2 = points[:,0].flatten()
            y1, y2 = points[:,1].flatten()
            theta = np.linspace(0, 2*np.pi, 100)
            radius = math.sqrt((x2 - x1)**2 + (y2 - y1)**2)/2 + 0.1
            center_x = sum(points[:,0])/2
            center_y = sum(points[:,1])/2
            x_circ = center_x + radius * np.cos(theta)
            y_circ = center_y + radius * np.sin(theta)
            dimred_fig.add_trace(go.Scatter(x=x_circ, y=y_circ, mode="lines", line=dict(dash="dash")))
        elif len(points) > 2: #If there's enough points for a convex hull, draw it
            hull = ConvexHull(points)
            hull_points = np.append(points[hull.vertices], [points[hull.vertices[0]]], axis=0)
            dimred_fig.add_trace(go.Scatter(x=hull_points[:,0], y=hull_points[:,1], 
                            mode='lines',line=dict(dash="dash"),hoverinfo='skip'))

    dimred_fig.update_traces(showlegend=False)
    dimred_fig.update_layout(yaxis_visible=True, yaxis_showticklabels=False, yaxis_title='',xaxis_visible=True, xaxis_showticklabels=False, xaxis_title='')
    dimred_fig.update_layout(coloraxis_colorbar_title_text = 'Metric') #TODO: make dynamicall
    dimred_fig.update_layout(height=300,margin=dict(l=0, r=0, t=10, b=10))
    return dimred_fig

# Performance matrix plot ###################################################################
@st.cache_data
def performance_matrix_plot(cluster_labels, performance_score_data, dimred_fig):
    clusters = np.unique(cluster_labels)

    cluster_performance_data = {model: [] for model in performance_score_data.columns}

    for cluster in clusters:
        cluster_data = performance_score_data.iloc[np.where(clusters==cluster)]
        for model, metric in cluster_data.mean().items():
            cluster_performance_data[model].append(metric)

    cluster_performance_matrix = pd.DataFrame(cluster_performance_data, index = [f"Cluster {cluster_id+1}" for cluster_id in clusters])
    cluster_performance_matrix = pd.concat([cluster_performance_matrix.T,cluster_performance_matrix.mean().rename('TOTAL')], axis=1).T
    cluster_performance_matrix = cluster_performance_matrix.sort_values(by='TOTAL', axis=1, ascending=True)
    cluster_performance_matrix['TOTAL'] = cluster_performance_matrix.mean(axis = 1)

    # Make the tooltip text
    tooltip_text = []
    n_clusters, models = cluster_performance_matrix.shape
    for c in range(n_clusters):
        row = []
        for m in range(models):
            cluster_info = f'Cluster {c+1}' if c < n_clusters-1 else 'Cluster Total'
            model_info = f'{cluster_performance_matrix.columns[m]}' if c < models-1 else 'Model Total'
            row.append(cluster_info + "<br>" + model_info + "<br>" + f"Score: {cluster_performance_matrix.iloc[c,m]:.2f}")
        tooltip_text.append(row)
    # Create heatmap with the same colorscale as the scatter plot
    clusmod_fig = go.Figure(data=[go.Heatmap(z=cluster_performance_matrix, x=cluster_performance_matrix.columns, y=cluster_performance_matrix.index,
                                    colorscale=dimred_fig.data[0].marker.colorscale)])
    clusmod_fig.update_traces(hovertemplate='%{text}', text=tooltip_text)

    clusmod_fig.update_layout( 
                    xaxis=dict(title='Models'), yaxis=dict(title='Clusters'))

    clusmod_fig.update_layout(xaxis_side='top', yaxis_autorange='reversed')
    clusmod_fig.update_layout(yaxis_title='', xaxis_title='')
    clusmod_fig.update_layout(coloraxis_colorbar_title_text = 'Metric') #TODO: make dynamicall
    # clusmod_fig.update_layout(xaxis=dict(ticktext=cluster_performance_matrix.columns),yaxis=dict(ticktext=cluster_performance_matrix.index))
    clusmod_fig.update_layout(height=75*len(clusters),margin=dict(l=0, r=0, t=10, b=10))

    return clusmod_fig
