from dash import Dash, dcc, html, Input, Output, ctx, no_update
import plotly.express as px
from config import *
from visualization import *
import pandas as pd
import dash_bootstrap_components as dbc
from dash.exceptions import PreventUpdate
from sklearn.metrics import silhouette_score


class AppStates:  # Defines the state the app is in
    INIT = 0
    CLUSTER_VIEW = 1
    MODEL_VIEW = 2
    SELECTION_VIEW = 3
    MODEL_CLUSTER_VIEW = 4
    MODEL_SELECTION_VIEW = 5


class InputTypes:
    CLUSTER = 0
    MODEL = 1
    SELECTION = 2


class DRTypes:
    PCA = "PCA"
    TSNE = "t-SNE"
    UMAP = "UMAP"


state_name = {
    0: "INIT",
    1: "CLUSTER_VIEW",
    2: "MODEL_VIEW",
    3: "SELECTION_VIEW",
    4: "MODEL_CLUSTER_VIEW",
    5: "MODEL_SELECTION_VIEW"
}

input_name = {
    0: "CLUSTER",
    1: "MODEL",
    2: "SELECTION"
}

# GLOBAL VARIABLES
# Specific dataset info
raw_data = performance_score_data = cluster_labels = cluster_count = dr_embeddings = None

# State variables
selected_points = previous_selection = current_selection = current_borders = current_cluster = current_model = current_embedding = None

cancel_update = False

DEBUG = True  # DEBUG

RANDOM_STATE = 1234

DATASET = "weather"
# DATASET = "finance"
# DATASET = "vaei"

app_state = AppStates.INIT


def clustering(raw_data, n_clusters=N_CLUSTERS_DEFAULT):
    # Number of clusters k is the square root of the number of points in the training data
    cluster_count = math.ceil(math.sqrt(len(raw_data)))
    cluster_count = min(n_clusters, cluster_count)

    scaler = StandardScaler()
    scaled_weather = scaler.fit_transform(raw_data)

    km = TimeSeriesKMeans(n_clusters=cluster_count, metric="dtw", n_init=10)

    cluster_labels = km.fit_predict(scaled_weather)

    return cluster_labels, cluster_count


def get_embeddings(raw_data):
    global cluster_labels, current_embedding

    # PCA embeddings
    pca_pipeline = Pipeline([
        ('scaler', StandardScaler()),
        ('dim_reduction', PCA(n_components=2, random_state=RANDOM_STATE))
    ])
    pca_embedding = pca_pipeline.fit_transform(raw_data)

    # t-SNE embeddings
    # define pipeline steps
    tsne_pipeline = Pipeline([
        ('scaler', StandardScaler()),
        ('2d_reduction',  TSNE(n_components=2, random_state=RANDOM_STATE))
    ])

    tsne_embedding = tsne_pipeline.fit_transform(raw_data)

    # UMAP embeddings
    umap_pipeline = Pipeline([
        ('scaler', StandardScaler()),
        ('dim_reduction', umap.UMAP(random_state=RANDOM_STATE))
    ])
    umap_embedding = umap_pipeline.fit_transform(raw_data)

    # Silhouette scores
    umap_score = silhouette_score(
        umap_embedding, cluster_labels, metric='euclidean')
    pca_score = silhouette_score(
        pca_embedding, cluster_labels, metric='euclidean')
    tsne_score = silhouette_score(
        tsne_embedding, cluster_labels, metric='euclidean')

    embedding_info = [(umap_embedding, umap_score),
                      (pca_embedding, pca_score),
                      (tsne_embedding, tsne_score)]

    embedding_info.sort(key=lambda x: x[1], reverse=True)

    current_embedding = embedding_info[0][0]

    return {
        DRTypes.PCA: (pca_embedding, pca_score),
        DRTypes.TSNE: (tsne_embedding, tsne_score),
        DRTypes.UMAP: (umap_embedding, umap_score),
    }

# To make tests for loading the necessary data for the app statically


def load_data_aux():
    global raw_data, performance_score_data, cluster_labels, cluster_count, dr_embeddings

    raw_data = pd.read_pickle(f'../Dataset/preprocessed/{DATASET}/X.pkl')

    performance_score_data = pd.read_pickle(
        f'../Dataset/performance_scores/error_{DATASET}.pkl')

    # Clustering
    cluster_labels, cluster_count = clustering(
        raw_data, n_clusters=N_CLUSTERS_DEFAULT)

    cluster_labels = cluster_labels
    cluster_count = cluster_count

    dr_embeddings = get_embeddings(raw_data)


load_data_aux()  # TODO: Eliminar cuando se cargue seleccionando archivos

app = Dash("ClAIrify", external_stylesheets=[dbc.themes.BOOTSTRAP])


def cluster_dropdown():
    global cluster_count
    layout = dcc.Dropdown(options=[{'label': f'Cluster {cid+1}', 'value': cid} for cid in range(
        cluster_count)], id="cluster-filter-dropdown", placeholder="Select a cluster")
    return layout


def model_dropdown():
    global performance_score_data
    layout = dcc.Dropdown([model for model in performance_score_data.columns],
                          id="model-filter-dropdown", placeholder="Select a model")
    return layout


def dimred_dropdown():
    global dr_embeddings

    embedding_info = [(method_name, embeddings, score)
                      for method_name, (embeddings, score) in dr_embeddings.items()]
    embedding_info.sort(key=lambda x: x[2], reverse=True)

    sorted_info = [(f"{method_name} - ({score:.4f})", method_name)
                   for (method_name, _, score) in embedding_info]

    layout = dcc.Dropdown(options=[{'label': label, 'value': method_name} for (label, method_name) in sorted_info], value=sorted_info[0][1],
                          id="dimred-filter-dropdown", clearable=False)
    return layout


def default_selection_stats():
    return """
    Make a selection to see relevant statistics here.
    """


def show_selection_stats(raw_data, selections=None, cluster=None):

    if selections is None:
        return default_selection_stats()

    stats = selection_stats(raw_data, selections)

    data_group = f"Cluster {cluster+1}" if cluster is not None else "Selection"

    markdown = f"""
    ### {data_group} stats:\n
    **Mean**: {stats['mean']:.4f}\n
    **Median**: {stats['median']:.4f}\n
    **Variance**: {stats['variance']:.4f}\n
    **Std. Dev**: {stats['std_dev']:.4f}\n
    **Skewness**: {stats['skewness']:.4f}\n
    **Kurtosis**: {stats['kurtosis']:.4f}\n
    """
    return markdown


def main_exploratory_view():
    dimred_fig = dimred_plot(performance_score_data,
                             cluster_labels, current_embedding)
    layout = dbc.Col([  # Content
        dbc.Row([
            dbc.Col(  # Performance matrix plot
                dcc.Graph(id='permat-plot', figure=performance_matrix_plot(cluster_labels, performance_score_data, dimred_fig)))
        ]),
        dbc.Row([
            dbc.Col(html.H5('DR Method'), width={'size': 2, 'offset': 2}),
            dbc.Col(dimred_dropdown(), width={'size': 2}),
            dbc.Col(html.H5('Filters'), width={'size': 2}),
            dbc.Col(cluster_dropdown(), width={'size': 2}),
            dbc.Col(model_dropdown(), width={'size': 2})
        ]),
        dbc.Row([
            dbc.Col(  # Cluster Plot
                dcc.Graph(id='cluster-plot', figure=cluster_plot(raw_data, cluster_labels, cluster_count)), width=4),
            dbc.Col([
                dbc.Row(
                    dbc.Col(  # Dimensionality reduction plot
                        dcc.Graph(id='dimred-plot',
                                  figure=dimred_fig)
                    )
                ),
                dbc.Row(
                    dbc.Col(  # Selection statistics
                        dcc.Markdown(default_selection_stats(
                        ), id="selection-stats-markdown")
                    )
                )
            ], width=8)
        ])
    ])

    return layout


# Contains the visual layout of the app
app.layout = dbc.Container([dbc.Row(dbc.Col([
    dbc.Row([
            dbc.Col(html.H1("ClAIrify"))]),  # Title
    main_exploratory_view()  # Main Page
    # ]))
], width={"size": 10, "offset": 1}))
], fluid=True)

# Function that takes an array of selected values and returns a boolean array that indexes into the overall df to retrieve the selected points


def extract_selected_values(selected_values):
    bool_index = np.zeros(len(raw_data), dtype=bool)
    selection_border = None
    if selected_values is None:
        return None, None

    if 'points' in selected_values and len(selected_values['points']) > 0:
        for p in selected_values['points']:
            if 'customdata' in p:
                bool_index[p['customdata']] = True

        selection_border = {
            'border_points': selected_values['range'] if 'range' in selected_values else selected_values['lassoPoints'],
            'type': 'square' if 'range' in selected_values else 'lasso'
        }

    else:
        return None, None

    return bool_index, selection_border


def clusteridx_to_selection(cluster_selection):
    global cluster_labels
    return cluster_labels == cluster_selection

# CALLBACKS


def default_view():
    return (
        cluster_plot(raw_data, cluster_labels, cluster_count),
        dimred_plot(performance_score_data, cluster_labels, current_embedding),
        show_selection_stats(raw_data)
    )


def cluster_view(cluster_selection):
    return (
        cluster_plot(raw_data, cluster_labels, cluster_count,
                     cluster_selection=cluster_selection),
        dimred_plot(performance_score_data, cluster_labels,
                    current_embedding, cluster_selection=cluster_selection),
        show_selection_stats(raw_data, clusteridx_to_selection(
            cluster_selection), cluster_selection)
    )


def update_model_view(model_selection):
    cluster_fig = no_update
    dimred_fig = dimred_plot(performance_score_data, cluster_labels,
                             current_embedding, model_selection=model_selection)
    stats_md = no_update

    return cluster_fig, dimred_fig, stats_md


def default_model_view(model_selection):
    cluster_fig = cluster_plot(raw_data, cluster_labels, cluster_count)
    dimred_fig = dimred_plot(performance_score_data, cluster_labels,
                             current_embedding, model_selection=model_selection)
    stats_md = show_selection_stats(raw_data)

    return cluster_fig, dimred_fig, stats_md


def model_cluster_view(cluster_selection, model_selection, input_type):

    cluster_fig = dimred_fig = stats_md = None

    if input_type == InputTypes.CLUSTER:
        cluster_fig = cluster_plot(
            raw_data, cluster_labels, cluster_count, cluster_selection=cluster_selection)
        dimred_fig = dimred_plot(performance_score_data, cluster_labels, current_embedding,
                                 cluster_selection=cluster_selection, model_selection=model_selection)
        stats_md = show_selection_stats(
            raw_data, clusteridx_to_selection(cluster_selection), cluster_selection)
    elif input_type == InputTypes.MODEL:
        cluster_fig = no_update
        dimred_fig = dimred_plot(performance_score_data, cluster_labels, current_embedding,
                                 cluster_selection=cluster_selection, model_selection=model_selection)
        stats_md = no_update

    return cluster_fig, dimred_fig, stats_md


def selection_view(selection, selection_border):
    cluster_fig = cluster_plot(
        raw_data, cluster_labels, cluster_count, selections=selection)
    dimred_fig = dimred_plot(performance_score_data, cluster_labels,
                             current_embedding, selection=selection, selection_border=selection_border)
    stats_md = show_selection_stats(raw_data, selections=selection)
    return cluster_fig, dimred_fig, stats_md


def model_selection_view(selection, selection_border, model_selection):
    cluster_fig = cluster_plot(
        raw_data, cluster_labels, cluster_count, selections=selection)
    dimred_fig = dimred_plot(performance_score_data, cluster_labels, current_embedding,
                             model_selection=model_selection, selection=selection, selection_border=selection_border)
    stats_md = show_selection_stats(raw_data, selection)

    return cluster_fig, dimred_fig, stats_md


def save_selections(new_selection, new_borders):
    global current_selection, current_borders

    current_selection = new_selection
    current_borders = new_borders


def load_selections():
    global current_selection, current_borders

    return current_selection, current_borders


@app.callback(
    Output('dimred-plot', 'figure', allow_duplicate=True),
    Input('dimred-filter-dropdown', 'value'),
    prevent_initial_call=True
)
def dimred_method_callback(dimred_method):
    global app_state, current_cluster, current_model, current_embedding

    selection, _ = load_selections()

    current_embedding = dr_embeddings[dimred_method][0]

    return dimred_plot(performance_score_data, cluster_labels, current_embedding,
                       model_selection=current_model, cluster_selection=current_cluster, selection=selection, selection_border=None)


@app.callback(
    Output('cluster-filter-dropdown', 'value'),
    Output('cluster-plot', 'figure'),
    Output('dimred-plot', 'figure'),
    Output('selection-stats-markdown', 'children'),
    Input('dimred-plot', 'selectedData'),
    Input('cluster-filter-dropdown', 'value'),
    Input('model-filter-dropdown', 'value'),
    prevent_initial_call=True
)
def crossfilter_callback(dimred_selection, cluster_selection, model_selection):
    global app_state, cancel_update, current_cluster, current_model

    if cancel_update:
        print("Cancelled Update")
        cancel_update = False
        raise PreventUpdate

    cluster_dd_value = cluster_fig = dimred_fig = stats_md = no_update

    # Get the input type
    input_type = None
    if ctx.triggered_id == 'cluster-filter-dropdown':
        input_type = InputTypes.CLUSTER
        previous_selection = None
        current_cluster = cluster_selection
    elif ctx.triggered_id == 'model-filter-dropdown':
        input_type = InputTypes.MODEL
        current_model = model_selection
    elif ctx.triggered_id == 'dimred-plot':
        input_type = InputTypes.SELECTION

    if DEBUG:
        print(f"State: {state_name[app_state]}, \
                Input: {input_name[input_type]}", end="")

    selection, selection_border = extract_selected_values(dimred_selection)

    if input_type == InputTypes.SELECTION:
        save_selections(selection, selection_border)

    if app_state == AppStates.INIT:
        if input_type == InputTypes.CLUSTER:
            if cluster_selection is None:
                raise PreventUpdate
            else:
                app_state = AppStates.CLUSTER_VIEW
                cluster_fig, dimred_fig, stats_md = cluster_view(
                    cluster_selection)
        elif input_type == InputTypes.MODEL:
            if model_selection is None:
                raise PreventUpdate
            else:
                app_state = AppStates.MODEL_VIEW
                cluster_fig, dimred_fig, stats_md = update_model_view(
                    model_selection)
        elif input_type == InputTypes.SELECTION:
            if selection is None:
                raise PreventUpdate
            else:
                app_state = AppStates.SELECTION_VIEW
                cancel_update = True
                cluster_fig, dimred_fig, stats_md = selection_view(
                    selection, selection_border)
    elif app_state == AppStates.CLUSTER_VIEW:
        if input_type == InputTypes.CLUSTER:
            if cluster_selection is None:
                app_state = AppStates.INIT
                cluster_fig, dimred_fig, stats_md = default_view()
            else:
                cluster_fig, dimred_fig, stats_md = cluster_view(
                    cluster_selection)
        elif input_type == InputTypes.MODEL:
            if model_selection is None:
                raise PreventUpdate
            else:
                app_state = AppStates.MODEL_CLUSTER_VIEW
                cluster_fig, dimred_fig, stats_md = model_cluster_view(
                    cluster_selection, model_selection, input_type)
        elif input_type == InputTypes.SELECTION:
            if selection is None:
                raise PreventUpdate
            else:
                app_state = AppStates.SELECTION_VIEW
                cancel_update = True
                cluster_dd_value = None
                cluster_fig, dimred_fig, stats_md = selection_view(
                    selection, selection_border)
    elif app_state == AppStates.MODEL_VIEW:
        if input_type == InputTypes.CLUSTER:
            if cluster_selection is None:
                raise PreventUpdate
            else:
                app_state = AppStates.MODEL_CLUSTER_VIEW
                cluster_fig, dimred_fig, stats_md = model_cluster_view(
                    cluster_selection, model_selection, input_type)
        elif input_type == InputTypes.MODEL:
            if model_selection is None:
                app_state = AppStates.INIT
                cluster_fig, dimred_fig, stats_md = default_view()
            else:
                cluster_fig, dimred_fig, stats_md = update_model_view(
                    model_selection)
        elif input_type == InputTypes.SELECTION:
            if selection is None:
                raise PreventUpdate
            else:
                app_state = AppStates.MODEL_SELECTION_VIEW
                # Cancel next update as the dimred_figure updates and triggers an event
                cancel_update = True
                cluster_fig, dimred_fig, stats_md = model_selection_view(
                    selection, selection_border, model_selection)
    elif app_state == AppStates.SELECTION_VIEW:
        if input_type == InputTypes.CLUSTER:
            if cluster_selection is None:
                raise PreventUpdate
            else:
                app_state = AppStates.CLUSTER_VIEW
                cluster_fig, dimred_fig, stats_md = cluster_view(
                    cluster_selection)
        elif input_type == InputTypes.MODEL:
            if model_selection is None:
                raise PreventUpdate
            else:
                app_state = AppStates.MODEL_SELECTION_VIEW
                selection, selection_border = load_selections()
                cluster_fig, dimred_fig, stats_md = model_selection_view(
                    selection, selection_border, model_selection)
        elif input_type == InputTypes.SELECTION:
            if selection is None:
                app_state = AppStates.INIT
                cluster_fig, dimred_fig, stats_md = default_view()
            else:
                cancel_update = True
                cluster_fig, dimred_fig, stats_md = selection_view(
                    selection, selection_border)
    elif app_state == AppStates.MODEL_CLUSTER_VIEW:
        if input_type == InputTypes.CLUSTER:
            if cluster_selection is None:
                app_state = AppStates.MODEL_VIEW
                cluster_fig, dimred_fig, stats_md = default_model_view(
                    model_selection)
            else:
                cluster_fig, dimred_fig, stats_md = model_cluster_view(
                    cluster_selection, model_selection, input_type)

        elif input_type == InputTypes.MODEL:
            if model_selection is None:
                app_state = AppStates.CLUSTER_VIEW
                cluster_fig, dimred_fig, stats_md = cluster_view(
                    cluster_selection)
            else:
                cluster_fig, dimred_fig, stats_md = model_cluster_view(
                    cluster_selection, model_selection, input_type)
        elif input_type == InputTypes.SELECTION:
            if selection is None:
                raise PreventUpdate
            else:
                app_state = AppStates.MODEL_SELECTION_VIEW
                cluster_dd_value = None
                # Cancel next update as the dimred_figure updates and triggers an event
                cancel_update = True
                cluster_fig, dimred_fig, stats_md = model_selection_view(
                    selection, selection_border, model_selection)
    elif app_state == AppStates.MODEL_SELECTION_VIEW:
        if input_type == InputTypes.CLUSTER:
            if cluster_selection is None:
                raise PreventUpdate
            else:
                app_state = AppStates.MODEL_CLUSTER_VIEW
                cluster_fig, dimred_fig, stats_md = model_cluster_view(
                    cluster_selection, model_selection, input_type)
        elif input_type == InputTypes.MODEL:
            selection, selection_border = load_selections()
            # cancel_update = True
            if model_selection is None:
                app_state = AppStates.SELECTION_VIEW
                cluster_fig, dimred_fig, stats_md = selection_view(
                    selection, selection_border)
            else:
                cluster_fig, dimred_fig, stats_md = model_selection_view(
                    selection, selection_border, model_selection)

        elif input_type == InputTypes.SELECTION:
            if selection is None:
                app_state = AppStates.MODEL_VIEW
                cluster_fig, dimred_fig, stats_md = default_model_view(
                    model_selection)
            else:
                # Cancel next update as the dimred_figure updates and triggers an event
                cancel_update = True
                cluster_fig, dimred_fig, stats_md = model_selection_view(
                    selection, selection_border, model_selection)

    if DEBUG:
        print(f' --> New State: {state_name[app_state]} -- Nº selection: {selection.sum() if selection is not None else "None"} | Cluster: {cluster_selection+1 if cluster_selection is not None else cluster_selection} | Model: {model_selection}')

    return cluster_dd_value, cluster_fig, dimred_fig, stats_md


if __name__ == '__main__':
    app.run_server(debug=True)
