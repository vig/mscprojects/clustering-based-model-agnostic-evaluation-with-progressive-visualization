from config import *
from pandas import DatetimeIndex

DEFAULT_PLOTLY_CLUSTER_COLORS = ['rgb(31, 119, 180)', 'rgb(255, 127, 14)',
                                 'rgb(44, 160, 44)', 'rgb(214, 39, 40)',
                                 'rgb(148, 103, 189)', 'rgb(140, 86, 75)',
                                 'rgb(227, 119, 194)', 'rgb(127, 127, 127)',
                                 'rgb(188, 189, 34)', 'rgb(23, 190, 207)']
DEFAULT_PLOTLY_FILL_CLUSTER_COLORS = ['rgba(31, 119, 180, 0.05)', 'rgba(255, 127, 14, 0.05)',
                                      'rgba(44, 160, 44, 0.05)', 'rgba(214, 39, 40, 0.05)',
                                      'rgba(148, 103, 189, 0.05)', 'rgba(140, 86, 75, 0.05)',
                                      'rgba(227, 119, 194, 0.05)', 'rgba(127, 127, 127, 0.05)',
                                      'rgba(188, 189, 34, 0.05)', 'rgba(23, 190, 207, 0.05)']

# METRIC = "RMSE (Fº)"
METRIC = "RMSE (MJ/USD)"

# Clustering plot ##########################################################################


def descriptive_stats_selection(selection):
    data = selection.T

    col1, col2, col3, col4, col5, col6 = st.columns(6)

    mean = data.values.mean()
    col1.metric(label="Mean", value=f"{mean:.4f}")
    median = data.stack().median()
    col2.metric(label="Median", value=f"{median:.4f}")
    variance = data.values.var()
    col3.metric(label="Variance", value=f"{variance:.4f}")
    std_dev = data.values.std()
    col4.metric(label="Std. dev", value=f"{std_dev:.4f}")
    skewness = data.stack().skew()
    col5.metric(label="Skewness", value=f"{skewness:.4f}")
    kurtosis = data.stack().kurtosis()
    col6.metric(label="Kurtosis", value=f"{kurtosis:.4f}")
    # autocorrelation = data.autocorr(lag=1)
    # col7.metric(label="Autocorrelation", value=f"{autorrelation}")


def cluster_plot(raw_data, cluster_labels, cluster_count, selections=None, cluster_selection=None):

    assert selections is None or cluster_selection is None

    lower_bound = raw_data.min().min()
    higher_bound = raw_data.max().max()

    # Convert Period objects to timestamps for PyPlot to be able to process them
    if type(raw_data.columns) is not DatetimeIndex:
        raw_data.columns = raw_data.columns.to_timestamp()

    n_rows = cluster_count if selections is None else cluster_count+1
    fig = make_subplots(rows=n_rows, cols=1,
                        vertical_spacing=0.015, shared_xaxes=True)

    if selections is not None:
        selected_data = raw_data[selections]
        for idx, row in selected_data.iterrows():
            fig.add_trace(go.Scatter(x=selected_data.columns, y=row,
                          line_color='rgba(30, 30, 30, 0.1)', hoverinfo='skip'), row=1, col=1)

        # Plot each cluster identifier
        barycenter = dtw_barycenter_averaging_subgradient(
            selected_data, max_iter=5).flatten()
        fig.add_trace(go.Scatter(x=selected_data.columns, y=barycenter.ravel(), line=dict(
            color=DEFAULT_PLOTLY_CLUSTER_COLORS[-1], width=7)), row=1, col=1)
        fig.update_yaxes(range=[lower_bound, higher_bound], row=1, col=1, title=f"Selection",
                         title_font_family="Arial", title_font_size=24, title_font_color=DEFAULT_PLOTLY_CLUSTER_COLORS[-1])

    for cluster in range(cluster_count):
        idxs = np.where(cluster_labels == cluster)[0]
        cluster_data = raw_data.iloc[idxs]
        # Plot each raw TS
        n_row = cluster + 1 if selections is None else cluster + 2
        for idx, row in cluster_data.iterrows():
            fig.add_trace(go.Scatter(x=cluster_data.columns, y=row,
                          line_color='rgba(30, 30, 30, 0.1)', hoverinfo='skip'), row=n_row, col=1)
        # Plot each cluster identifier
        barycenter = dtw_barycenter_averaging_subgradient(
            cluster_data, max_iter=5).flatten()
        fig.add_trace(go.Scatter(x=cluster_data.columns, y=barycenter.ravel(), line=dict(
            color=DEFAULT_PLOTLY_CLUSTER_COLORS[cluster], width=7)), row=n_row, col=1)
        fig.update_yaxes(range=[lower_bound, higher_bound], row=n_row, col=1,
                         title=f"Cluster {cluster+1}", title_font_family="Arial", title_font_size=24, title_font_color=DEFAULT_PLOTLY_CLUSTER_COLORS[cluster])
        if cluster_selection is not None:  # For highlighting the selected cluster's subplot
            if cluster_selection == cluster:
                fig.add_shape(type='rect', xref='paper', yref=f'y{cluster+1} domain' if cluster > 0 else 'y domain',
                              x0=0, x1=1, y0=0, y1=1, layer='below', line_width=4, line_color=DEFAULT_PLOTLY_CLUSTER_COLORS[cluster])

                fig.add_shape(type='rect', xref='paper', yref=f'y{cluster+1} domain' if cluster > 0 else 'y domain',
                              x0=0, x1=1, y0=0, y1=1, layer='below', line_width=6, line_color=DEFAULT_PLOTLY_CLUSTER_COLORS[cluster], opacity=0.5)

    fig_height = cluster_count * \
        150 if selections is None else (cluster_count+1)*150
    fig.update_layout(height=fig_height, margin=dict(
        l=50, r=0, t=10, b=30), yaxis=dict(showticklabels=True))
    # fig.update_layout(height=fig_height, yaxis=dict(showticklabels=True))
    fig.update_traces(showlegend=False,
                      hovertemplate="y: %{y} x: %{x} <extra></extra>")

    return fig


def dimred_plot(performance_score_data, clusters, embeddings, cluster_selection=None, model_selection=None, selection=None, selection_border=None):

    performance_score_data = performance_score_data.mean(
        axis=1).values if model_selection is None else performance_score_data[model_selection].values

    # Create a scatterplot with different colors for each cluster
    dimred_fig = go.Figure()

    dimred_fig.add_trace(go.Scatter(mode='markers', x=embeddings[:, 0], y=embeddings[:, 1],
                                    marker={'color': performance_score_data, 'symbol': clusters, 'colorscale': COLOR_SCALE, "colorbar": {"thickness": 20, "title": METRIC, 'titleside': 'top'}}))

    dimred_fig.update_traces(hovertemplate='%{text} <extra></extra>',
                             text=[f'Score: {performance_score:.2f}<br>Cluster: {cluster_id+1}' for performance_score,
                                   cluster_id in zip(performance_score_data, clusters)],
                             marker=dict(colorscale=COLOR_SCALE),
                             customdata=list(range(len(embeddings))),
                             selectedpoints=np.where(selection)[
                                 0] if selection is not None else None,
                             unselected={"marker": {"opacity": 0.3}, "textfont": {"color": "rgba(0, 0, 0, 0)"}, })

    # Compute the convex hulls of each cluster and add them to the plot
    for cluster_label in np.unique(clusters):
        points = embeddings[clusters == cluster_label]
        hull = ConvexHull(points)
        hull_points = np.append(points[hull.vertices], [
                                points[hull.vertices[0]]], axis=0)
        dimred_fig.add_trace(go.Scatter(x=hull_points[:, 0], y=hull_points[:, 1], mode='lines', fill='toself', line={
                             'width': 1, 'color': DEFAULT_PLOTLY_CLUSTER_COLORS[cluster_label]}, fillcolor=DEFAULT_PLOTLY_FILL_CLUSTER_COLORS[cluster_label], hoverinfo='skip'))
        if cluster_selection is not None and cluster_selection == cluster_label:
            dimred_fig.add_trace(go.Scatter(x=hull_points[:, 0], y=hull_points[:, 1], mode='lines', fill='toself', line={
                                 'width': 3, 'color': DEFAULT_PLOTLY_CLUSTER_COLORS[cluster_label]}, fillcolor=DEFAULT_PLOTLY_FILL_CLUSTER_COLORS[cluster_label], hoverinfo='skip', opacity=0.5))

    if selection_border is not None:
        if selection_border['type'] == 'square':
            selection_bounds = {
                "x0": selection_border['border_points']["x"][0],
                "x1": selection_border['border_points']["x"][1],
                "y0": selection_border['border_points']["y"][0],
                "y1": selection_border['border_points']["y"][1],
            }
            dimred_fig.add_shape(dict({"type": "rect", "line": {"width": 2, "dash": "dot", "color": "black"}}, **selection_bounds)
                                 )
        elif selection_border['type'] == 'lasso':
            border_points = selection_border['border_points']
            dimred_fig.add_trace(go.Scatter(x=border_points['x'] + [border_points['x'][0]], y=border_points['y'] + [
                                 border_points['y'][0]], mode='lines', line={'width': 2, "dash": "dot", "color": "black"}, hoverinfo='skip'))

    dimred_fig.update_traces(showlegend=False)
    dimred_fig.update_layout(yaxis_visible=True, yaxis_showticklabels=False,
                             yaxis_title='', xaxis_visible=True, xaxis_showticklabels=False, xaxis_title='')
    dimred_fig.update_layout(coloraxis_colorbar_title_text='Metric')
    dimred_fig.update_layout(height=300, margin=dict(l=0, r=0, t=10, b=10))
    return dimred_fig


# Performance matrix plot ###################################################################

def performance_matrix_plot(cluster_labels, performance_score_data, dimred_fig):
    clusters = np.unique(cluster_labels)

    cluster_performance_data = {model: []
                                for model in performance_score_data.columns}

    for cluster in clusters:
        cluster_data = performance_score_data.iloc[np.where(
            cluster_labels == cluster)]
        for model, metric in cluster_data.mean().items():
            cluster_performance_data[model].append(metric)

    cluster_performance_matrix = pd.DataFrame(cluster_performance_data, index=[
                                              f"Cluster {cluster_id+1}" for cluster_id in clusters])
    cluster_performance_matrix = pd.concat(
        [cluster_performance_matrix.T, cluster_performance_matrix.mean().rename('TOTAL')], axis=1).T
    cluster_performance_matrix = cluster_performance_matrix.sort_values(
        by='TOTAL', axis=1, ascending=True)
    cluster_performance_matrix['TOTAL'] = cluster_performance_matrix.mean(
        axis=1)

    # Make the tooltip text
    tooltip_text = []
    n_clusters, models = cluster_performance_matrix.shape
    for c in range(n_clusters):
        row = []
        for m in range(models):
            cluster_info = f'Cluster {c+1}' if c < n_clusters - \
                1 else 'Cluster Total'
            model_info = f'{cluster_performance_matrix.columns[m]}' if c < models - \
                1 else 'Model Total'
            row.append(cluster_info + "<br>" + model_info + "<br>" +
                       f"Score: {cluster_performance_matrix.iloc[c,m]:.2f}")
        tooltip_text.append(row)

    # Create heatmap with the same colorscale as the scatter plot
    clusmod_fig = go.Figure(data=[go.Heatmap(z=cluster_performance_matrix, x=cluster_performance_matrix.columns, y=cluster_performance_matrix.index,
                                             colorscale=dimred_fig.data[0].marker.colorscale, colorbar={"title": METRIC, 'titleside': 'top'}, texttemplate="%{z:.2f}")])
    clusmod_fig.update_traces(
        hovertemplate='%{text} <extra></extra>', text=tooltip_text)

    clusmod_fig.update_layout(xaxis_side='top', yaxis_autorange='reversed')
    clusmod_fig.update_layout(
        yaxis_title='', xaxis_title='', margin=dict(b=10))
    clusmod_fig.update_layout(coloraxis_colorbar_title_text='Metric')

    return clusmod_fig


def selection_stats(raw_data, selections=None):
    if selections is not None:
        selected_data = raw_data[selections]
    else:
        return None

    mean = selected_data.values.mean()
    median = selected_data.stack().median()
    variance = selected_data.values.var()
    std_dev = selected_data.values.std()
    skewness = selected_data.stack().skew()
    kurtosis = selected_data.stack().kurtosis()
    return {
        "mean": mean,
        "median": median,
        "variance": variance,
        "std_dev": std_dev,
        "skewness": skewness,
        "kurtosis": kurtosis
    }
