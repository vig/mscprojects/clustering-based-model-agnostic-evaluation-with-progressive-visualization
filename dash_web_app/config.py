import json
import copy
import statsmodels.api as sm
from plotly.subplots import make_subplots
import plotly.express as px
import plotly.graph_objects as go
from sklearn.preprocessing import StandardScaler
from scipy import stats
from scipy.spatial import ConvexHull
from tslearn.clustering import TimeSeriesKMeans
from tslearn.barycenters import dtw_barycenter_averaging_subgradient
# from matplotlib.lines import Line2D
# import matplotlib.markers as markers
# from matplotlib.colors import TABLEAU_COLORS
# import matplotlib.pyplot as plt
import math
import numpy as np
import pandas as pd
from sklearn.pipeline import Pipeline
import umap
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE


N_CLUSTERS_DEFAULT = 5

COLOR_SCALE = 'fall'

# # MARKER_SHAPES = list(markers.MarkerStyle.markers.keys())[2:37]
# CLUSTER_COLORS = {cluster_id: color for cluster_id, color in zip(
#     range(len(TABLEAU_COLORS)), TABLEAU_COLORS.values())}
# CLUSTER_POINT_SHAPE = {cluster_id: shape for cluster_id,
#                        shape in zip(range(len(MARKER_SHAPES)), MARKER_SHAPES)}


TMP_FOLDER = "./tmp/"
TMP_CLUSTER_FILE = TMP_FOLDER + "clusters.pkl"
